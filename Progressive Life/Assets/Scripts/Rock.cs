﻿using UnityEngine;
using System.Collections;

public class Rock: Environment {

	public Sprite dmgSprite;
	public int cantidad;

	private SpriteRenderer sRenderer;


	protected override void Awake() {
		base.Awake ();
		sRenderer = GetComponent<SpriteRenderer> ();

	}


	public override void Interact() {
		infoText.setText("Una roca, al picarla se obtiene piedra");

	}

	public override void Gather () {
		float dist = Vector3.Distance(Player.instance.transform.position, transform.position);
		if (dist < minInteractDistance) {
			Player.instance.GetComponent<Inventory>().addItem("Piedra", cantidad);
			infoText.setText ("Piedra +" + cantidad);
		}
		else
			infoText.setText("Estoy demasiado lejos");

	}


	/*
	public override void Hit(int dmg) {
		sRenderer.sprite = dmgSprite;
		vida -= dmg;
		print ("Al arbol le queda " + vida + " de vida");
		if (vida <= 0) {
			gameObject.SetActive (false);
			//Instantiate (log, transform.position, Quaternion.identity);

		}

	}
	*/






}
