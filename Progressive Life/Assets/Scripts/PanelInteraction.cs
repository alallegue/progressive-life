﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.UI;

public class PanelInteraction : MonoBehaviour {

	//private Environment entity;  //Guarda una instancia del objeto que va a usar el panel


	void Awake() {
		print ("Start panel interaction");
	}

	void Start () {
		
	}
	
	void Update () {

	}

	/*
	public void setGameObject(MonoBehaviour obj) {
		if (obj.CompareTag ("Environment")) {
			entity = obj as Environment;
			print ("linkeado objeto con menu");
		}

	}
	*/
	/*
	public void InteractListener() {
		entity.Interact ();
	}
	*/

	public void Interact() {
		//print ("Interactuando con " + GameManager.instance.getLastEnv ().name);
		GameManager.instance.getLastEnv ().Interact ();

	}

	public void Gather() {
		//print ("Recolectando " + GameManager.instance.getLastEnv ().name );
		GameManager.instance.getLastEnv ().Gather();
	}

	public void Damage() {
		//Debug.Log ("Destruir " + GameManager.instance.getLastEnv ().name);
		GameManager.instance.getLastEnv ().Damage ();
	}

}
