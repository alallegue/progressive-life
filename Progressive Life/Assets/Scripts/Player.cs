﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	//Instancia Singleton
	public static Player instance = null;

	//Parámetros
	public int vida;
	public int hambre;
	public float velocidad;
	public int daño;
	public float periodoHambre; //Cada cuantos segundos se pierde vida por hambre
	public int hambrePorPeriodo; //Cuantos puntos de hambre se pierden por periodo
	//public int dañoPorHambre; //Cuantos puntos de vida se pierden cuando no queda de hambre

	public Slider barraVida;
	public Slider barraHambre;
	public InfoText infoText;

	//Componentes
	private Rigidbody2D rb2d;
	private Animator animator;
	private SpriteRenderer spriteRenderer;
	private BoxCollider2D selfCollider;

	private Vector2 direction;
	//private Inventory inventory;
	private float hungerTime;
	private GameObject camara;

	void Awake() {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
		
		rb2d = gameObject.GetComponent<Rigidbody2D> ();
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
		selfCollider = gameObject.GetComponent<BoxCollider2D> ();

		direction = new Vector2 (0, 1);
		//inventory = gameObject.GetComponent<Inventory> ();
		hungerTime = 0;
		camara = GameObject.Find ("Camara");
	}

	void Start() {
		barraVida.maxValue = vida;
		barraVida.value = vida;
		barraHambre.maxValue = hambre;
		barraHambre.value = hambre;

	}

	void FixedUpdate() {
		Move ();

	}

	void Update() {
		
		checkHunger ();

		checkInput ();

	}


	/// Mover al personaje con WASD o flechas de dirección
	/// Actualizar animación según la dirección del personaje
	void Move() {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		rb2d.velocity = new Vector2 (moveHorizontal * velocidad, moveVertical * velocidad);

		direction.x = moveHorizontal > 0 ? 1 : -1; 
		direction.y = moveVertical > 0 ? -1 : 1;


		camara.transform.position = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y, camara.transform.position.z);

		//Animacion
		/*
		if (moveHorizontal != 0 || moveVertical != 0)
			animator.SetBool ("Move", true);
		else
			animator.SetBool ("Move", false);

		spriteRenderer.flipX = moveHorizontal >= 0 ? true : false; 
		*/

	}

	private void checkHunger() {
		//Actualizar tiempos
		hungerTime += Time.deltaTime;
		if (hungerTime >= periodoHambre) {
			hungerTime = 0;
			hambre -= hambrePorPeriodo;
			// Si no tiene puntos de hambre, pierde puntos de vida
			if (hambre < 0) {
				vida += hambre; 
				hambre = 0;
			}
			Debug.Log ("Vida: " + vida + " Hambre: " + hambre);

			barraHambre.value = hambre;
			barraVida.value = vida;
			Debug.Log (barraHambre.value);

			//Mostrar mensaje en la interfaz
			infoText.setText("Vida: " + vida + " Hambre: " + hambre);


		}
	}

	private void checkInput() {
		//Ocultar menu de interaccion al pulsar espacio
		if (Input.GetKeyDown(KeyCode.Space)) {
			GameManager.instance.hideMenuPanel ();
		}
	}


}
