﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



/*
 * Constructor
 * getItem(tag)
 * List empty()
 * isFull()
 * insertItem(Item)
 * 
*/

public class Inventory : MonoBehaviour{

	/* Inventario avanzado
	//[Serializable]
	public class Item {
		public int cantidad;
		public bool consumible;

		public Item(int cantidad, bool consumible) {
			this.cantidad = cantidad;
			this.consumible = consumible;
		}
	}
	*/

	private Dictionary<string, int> items;
	//private Dictionary<string, Item> items;


	void Awake() {
		print ("Init inventory");
		items = new Dictionary<string, int> ();
	}


	public void addItem(string item, int cuantity) {
		if (items.ContainsKey (item))
			items [item] += cuantity;
		else
			items.Add (item, cuantity);

		printInventory ();
	}


	public void printInventory() {
		foreach (string s in items.Keys) {
			Debug.Log(s + " : " + items [s]);
		}
	}
}
