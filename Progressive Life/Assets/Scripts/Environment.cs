﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class Environment : MonoBehaviour {

	public int vida;
	public InfoText infoText;

	protected BoxCollider2D boxCollider;
	protected int minInteractDistance = 1;


	protected virtual void Awake() {
		boxCollider = GetComponent<BoxCollider2D> ();

	}


	protected virtual void Start() {}

	protected virtual void FixedUpdate() {}

	protected virtual void Update() {}


	///<summary>
	/// Detectar colisión con otro BoxCollider2D
	/// </summary>
	/// <param name="coll">Colisión del otro objeto</param>
	void OnCollisionEnter2D(Collision2D coll) {
		print ("Me colisiona un " + coll.transform.tag);
	}


	//Metodos abstractos a implementar

	///<summary>
	/// Reacción al pulsar interactuar en el menu de interaccion
	/// Muestra por consola una pequeña descripción del objeto
	/// </summary>
	public abstract void Interact ();

	/// <summary>
	/// Reaccion al pulsar recolectar en el menu de interaccion
	/// Recolecta el objeto, si tiene, y lo añade al inventario del jugador
	/// </summary>
	public abstract void Gather ();

	/// <summary>
	/// Daña al objeto, que será destruido si su vida se reduce a 0.
	/// </summary>
	/// <param name="dmg">Daño a aplicar</param>
	public virtual void Damage(int dmg = 0) {
		float dist = Vector3.Distance(Player.instance.transform.position, transform.position);
		if (dist < minInteractDistance) {
			if (dmg > 0) {
				//Aplicar daño indicado
				vida -= dmg;
			} else {
				//Aplicar daño del jugador
				vida -= Player.instance.daño;
			}
			if (vida <= 0) {
				GameManager.instance.hideMenuPanel ();
				Destroy (gameObject);
				//gameObject.SetActive(false);
			}
		} else {
			//infoText.GetComponent<InfoText> ().setText ("
			infoText.setText("Estoy demasiado lejos");
			Debug.Log ("Demasiado lejos: " + dist);
		}
	}

	/// Detectar selección del jugador con click derecho sobre este objeto
	/// para mostrar/ocultar el menu de interaccion
	void OnMouseOver() {
		if (Input.GetMouseButtonDown (1)) {
			GameManager.instance.handlePanelMenu (this);
		}
	}



}
