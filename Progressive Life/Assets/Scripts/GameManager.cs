﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null;
	public GameObject panelInteraction; //Controlador del panel, con sus acciones para los metodos onclick de los botones
	public GameObject panelMenu; //Objeto PanelInteraction de UI

	private GameObject canvas;
	private Camera camara;

	private Environment lastEnv;



	void Awake () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
		//DontDestroyOnLoad (gameObject); //Asegurar la persistencia del objeto entre escenas

		camara = gameObject.GetComponent<Camera> ();

		canvas = (GameObject)GameObject.Find ("Canvas");
		if (canvas == null)
			Debug.Log ("La escena requiere del prefab Canvas para su funcionamiento");
		
		panelMenu = (GameObject)Instantiate (panelMenu);
		panelMenu.transform.SetParent (canvas.transform, false);
		//panelMenu.GetComponent<PanelInteraction> ().setGameObject(this);
		panelMenu.SetActive (false);

	}

	void FixedUpdate() {
		
	}

	void Update() {
		
	}

	/// Obtiene el ultimo objeto Environment seleccionado por el jugador.
	/// Para que el menú pueda interaccionar con dicho objeto
	/// <returns>Ultimo Environment seleccionado</returns>
	public Environment getLastEnv() {
		return lastEnv;
	}

	/// Oculta el menú de interacción y elimina el último objeto seleccionado.
	public void hideMenuPanel() {
		panelMenu.SetActive (false);
		lastEnv = null;
	}
		
	/// Muestra u oculta el menu de interacción en la posición del objeto especificado.
	/// <param name="objCall">Instancia del objeto que requiere desplegar el menu</param>
	public void handlePanelMenu(Environment objCall) {
		if (objCall != null) {
			if (panelMenu.activeSelf) {
				panelMenu.SetActive (false);
				lastEnv = null;
			} else {
				lastEnv = objCall;
				panelMenu.SetActive (true);
				panelMenu.transform.position = RectTransformUtility.WorldToScreenPoint(camara, objCall.transform.position);
			}
		} 
	}


}
