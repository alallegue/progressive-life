﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

	public Sprite openedSprite;

	private BoxCollider2D coll;
	private SpriteRenderer spriteRenderer;
	private Sprite closedSprite;
	protected int minInteractDistance = 3;

	void Start() {
		coll = gameObject.GetComponent<BoxCollider2D> ();
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
		closedSprite = spriteRenderer.sprite;
	}

	void OnMouseOver() {
		if(Input.GetMouseButtonDown(0)) {
			if (Vector3.Distance (Player.instance.transform.position, transform.position) < minInteractDistance) {
				coll.isTrigger = !coll.isTrigger;
				if (coll.isTrigger)
					spriteRenderer.sprite = openedSprite;
				else
					spriteRenderer.sprite = closedSprite;
			}
		}
	}

}
