﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoText : MonoBehaviour {

	public float tiempo; //Periodo de tiempo de muestra

	private Text text;
	private float showTime; //Tiempo restante mostrando el mensaje

	// Use this for initialization
	void Start () {
		text = gameObject.GetComponent<Text> ();
		showTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (showTime > 0)
			showTime -= Time.deltaTime;
		else
			text.text = "";

	}


	/*
	//Usar temporizador casero en vez de corutina para mejor control sobre los mensajes
	private IEnumerator setMessageText(string m) {
		text.text = m;
		yield return new WaitForSeconds (tiempo);
		text.text = "";
	}
	*/

	public void setText(string t) {
		//StartCoroutine("setMessageText", t);
		text.text = t;
		showTime = tiempo;
	}
}
