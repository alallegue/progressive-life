﻿using UnityEngine;
using System.Collections;

public class Tree : Environment {

	public Sprite dmgSprite;
	public int cantidad;

	private SpriteRenderer sRenderer;


	protected override void Awake() {
		base.Awake ();
		sRenderer = GetComponent<SpriteRenderer> ();

	}


	public override void Interact() {
		infoText.setText("Un arbol, al talarlo se obtiene madera");
	}

	public override void Gather() {
		//Comprobar si el jugador está lo suficientemente cerca del objeto
		float dist = Vector3.Distance(Player.instance.transform.position, transform.position);
		if (dist < minInteractDistance) {
			Player.instance.GetComponent<Inventory> ().addItem ("Madera", cantidad);
			infoText.setText("Madera +" + cantidad);
		}
		else
			infoText.setText("Estoy demasiado lejos");
	}


	/*
	public override void Damage(int dmg) {
		if (dmg > 0) {
			//Aplicar daño indicado
			vida -= dmg;
		} else {
			//Aplicar daño del jugador
			vida -= Player.instance.daño;
		}
		if (vida <= 0)
			Destroy ();

	}
	*/

	/*
	/// <summary>
	/// Initializes a new instance of the <see cref="Tree"/> class.
	/// </summary>
	/// <param name="dmg">Dmg.</param>
	public override Hit(int dmg) {
		//Actualizar el sprite || Animacion
		//sRenderer.sprite = dmgSprite;

		vida -= dmg;
		print ("Al arbol le queda " + vida + " de vida");
		if (vida <= 0) {
			gameObject.SetActive (false);
			Instantiate (log, transform.position, Quaternion.identity);

		}
	}
	*/


}
